# RTSP.io Proxy Server JavaScript Browser Client #

Please report all bugs in the [issue tracker](https://bitbucket.org/aceso/rtsp.io-client-js/issues?status=new&status=open).

This library provides an interface to the RTSP.io Proxy Server. This package includes a simulator for testing and its usage will be outlined below. Before getting started, there are some things to know.

[grunt-cli](http://gruntjs.com/getting-started) is required. As root (or Administrator in Windows) run:

```
npm install -g grunt-cli
```

Though not required, it is super handy. [Nodemon](http://nodemon.io/) will restart the application whenever there is a change to the code. Starting this process will be mentioned below. As root (or Administrator in Windows) run:

```
npm install -g nodemon
```
 
Copy grunt.config-default.json to grunt.config.json and fill in properties, if necessary. Creating grunt.config.json is necessary but filling property valuees is not.

Once cloned to your local computer, you will need to run:
  
```
npm install
```

Within the project root to install all dependencies. Once all dependencies are installed, you will need to run a grunt command within the project root to do the initial build:

```
grunt build
```

We also have some grunt watch tasks. These come in handy when working on the project as they do many things

1. rebuild client.js and copies the compiled source to the public directory for inclusion in the simulator
1. watches for changes in ejs, css and other js and reloads the browser

To start this process, from the project root run:


```
grunt watch
```

From the project root, we are now ready to start the simulator. Using node:

```
node app.js
```

...or, you can use nodemon:

```
nodemon app.js
```

To access the simulator, in your browser, go to [http://localhost:3000](http://localhost:3000)