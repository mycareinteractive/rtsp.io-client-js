﻿(function () {
    "use strict";
}());

var player = {
    state: null,
    seconds: 0,
    duration: 99999,
    scale: 1.0,
    ip: null,
    device: null,
    updateTimer: null,
    keepAliveTimer: null,

    rtspUrlMask: "rtsp://%serverIp%:%serverPort%/%appId%?assetUid=%assetUid%&transport=%transport%&ServiceGroup=%serviceGroup%&smartcard-id=%deviceId%&device-id=%deviceId%&home-id=%homeId%&client_mac=%deviceId%&purchase-id=%ticketId%",

    progress: function () {
        if (this.state === 'play') {
            this.seconds += this.scale * 1;
        }
        var percent = Math.floor(this.seconds / this.total * 100);
        $('#video-progress').text(this.seconds).css('width', percent + '%').attr('aria-valuenow', percent);
    },

    load: function () {
        var self = this;
        var url = $('#movie option:selected').val();

        var homeId = "101";
        var device = "000000000000";

        this.ip = $('#ipaddress').val(); // + ":1235";
        RTSPIOClient.setIp(this.ip);
        RTSPIOClient.setRoomNumber(homeId);
        RTSPIOClient.setDevice(device);
        RTSPIOClient.setUrl(url);
        RTSPIOClient.load(url);
    },

    play: function (scale, range) {
        var self = this;
        RTSPIOClient.play(scale, range);

        clearInterval(this.updateTimer);
        this.updateTimer = setInterval(function () {
            self.progress();
        }, 1000);
        clearInterval(this.keepAliveTimer);
        this.keepAliveTimer = setInterval(function () {
            self.currentTime();
        }, 30000);

        this.state = "play";
    },

    stop: function () {
        RTSPIOClient.teardown();
        //RTSPIOClient.disconnect();
        this.state = null;
    },

    pause: function () {
        RTSPIOClient.pause();
        this.state = 'pause';
    },

    resume: function (restart) {
        var range = restart ? '0.0-' : this.seconds + '-';
        if (this.seconds < 5.0) {    // beginning
            range = '0.0-';
        }
        RTSPIOClient.play('1.0', range);
        this.state = 'play';
        this.scale = 1.0;
    },

    fastforward: function () {
        RTSPIOClient.play("7.5");
        this.scale = 7.5;
    },

    rewind: function () {
        RTSPIOClient.play("-7.5");
        this.scale = -7.5;
    },

    currentTime: function () {
        RTSPIOClient.getCurrentTime();
    },

    init: function () {
        var self = this;

        // wire up some events

        // This is fired up when socket.io client js is loaded and socket object created (not connected yet)
        document.addEventListener('rtspio.initialized', function (e) {
            $('.video-ops').show();
        });

        document.addEventListener('rtspio.endOfStream', function (e) {
            self.stop();
            debug.log('we have reached end of the stream');
        }, false);

        document.addEventListener('rtspio.beginningOfStream', function (e) {
            self.play();
            debug.log('we have reached the beginning of the stream');
        }, false);

        // This is fired up when stream loaded, ready to play
        document.addEventListener('rtspio.loaded', function (e) {
            debug.log("we are loaded");
            self.play('1.0', "0.0-");
        });

        document.addEventListener('rtspio.teardown', function (e) {
            debug.log('we are torn down');
            self.kill();
        }, false);

        document.addEventListener('rtspio.disconnect', function (e) {
            debug.log('we are disconnected');
            self.kill();
        }, false);

        document.addEventListener('rtspio.currentTime', function (e) {
            debug.log('current time = ' + JSON.stringify(e.param));
            var npt = e.param['Position'].split('-');
            self.seconds = Math.floor(npt[0] * 1);
            self.total = Math.floor(npt[1] * 1);
        });

        document.addEventListener('rtspio.playing', function (e) {
            debug.log('we are playing now');
            self.currentTime();
        });

        // init client library.  This will load necessary socket.io client js but will not establish the connection.
        RTSPIOClient.init({
            proxyServerUrl: 'http://localhost:1337',
        });
    },

    kill: function () {
        clearInterval(this.updateTimer);
        clearInterval(this.keepAliveTimer);
        RTSPIOClient.kill();
        this.state = null;
        //this.seconds = 0.0;
        this.scale = 1.0;
    }
};

$(document).ready(function () {

    $('.video-ops').hide();

    player.init();

    $('#btn_play').click(function (e) {
        if (!player.state) {
            player.load();
        }
        else {
            player.resume();
        }
        e.preventDefault();
    });

    $('#btn_pause').click(function (e) {
        player.pause();
        e.preventDefault();
    });

    $('#btn_stop').click(function (e) {
        player.stop();
        e.preventDefault();
    });

    $('#btn_forward').click(function (e) {
        player.fastforward();
        e.preventDefault();
    });

    $('#btn_rewind').click(function (e) {
        player.rewind();
        e.preventDefault();
    });

    $('#btn_currentTime').click(function (e) {
        player.currentTime();
        e.preventDefault();
    });

});