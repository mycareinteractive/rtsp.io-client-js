(function () {
    "use strict";
}());
// Dummy debug object
var dummyDebug = {};
dummyDebug.log = dummyDebug.trace = dummyDebug.status = function (msg) {
    console.log(msg);
};
var debug = window.debug || dummyDebug;

var RTSPIOClient = {

    socket: null,
    url: null,

    proxyServerUrl: '',
    ip: '',
    device: '',
    roomNumber: '',

    events: {
        initialized: 'rtspio.initialized',
        beginningOfStream: 'rtspio.beginningOfStream',
        endOfStream: 'rtspio.endOfStream',
        error: 'rtspio.error',
        playing: 'rtspio.playing',
        loaded: 'rtspio.loaded',
        paused: 'rtspio.paused',
        currentTime: 'rtspio.currentTime',
        announced: 'rtspio.announced',
        teardown: 'rtspio.teardown',
        disconnect: 'rtspio.disconnect'
    },

    connect: function () {
        debug.log('proxy client connect');
        this.socket = this.io(this.proxyServerUrl, {forceNew: true});
        this.initListeners();
        if (!this.socket.connected) {
            this.socket.connect(this.proxyServerUrl);
            debug.log('connecting to ' + this.proxyServerUrl);
        }
    },

    disconnect: function () {
        if(this.socket) {
            this.socket.off('message');
            this.socket.off('rtspio.handshake');
            this.socket.off('rtspio.ended');
            this.socket.off('rtspio.loaded');
            this.socket.off('rtspio.playing');
            this.socket.off('rtspio.paused');
            this.socket.off('rtspio.currentTime');
            this.socket.off('rtspio.announced');
            this.socket.off('rtspio.teardown');
            this.socket.off('rtspio.error');
            this.socket.disconnect();
            delete this.socket;
            this.socket = null;
            this.url = null;
            debug.log('proxy disconnected!');
        }
    },

    initEventObj: function (evt, props) {
        var e = new Event(evt);
        for (var p in props) {
            e[p] = props[p];
        }
        return e;
    },

    initListeners: function () {
        var self = this;

        // socket.io 'io' object events
        this.socket.once('connect', function () {
            debug.log('proxy connected successfully!');
            // once connected, we will do handshake then load...etc.
            self._emitHandshake();
        });

        this.socket.once('disconnect', function () {
            debug.log('proxy disconnected!');
            var e = self.initEventObj(self.events.disconnect, {});
            document.dispatchEvent(e);
        });

        this.socket.on('message', function (data) {
            debug.log(data);
        });

        this.socket.on('connect_error', function (e) {
            debug.log('proxy connection error! ' + e);
        });

        this.socket.on('reconnect', function () {
            debug.log('proxy re-connected successfully!');
        });

        this.socket.on('reconnecting', function () {
            debug.log('proxy re-connecting...');
        });

        this.socket.on('reconnect_error', function (e) {
            debug.log('proxy re-connection error! ' + e);
        });

        this.socket.on('rtspio.handshake', function () {
            debug.log('handshake event');
            self._emitLoad();
        });

        // rtsp.io server custom events
        this.socket.on('rtspio.error', function (code, notice, param) {
            debug.log(code);
            debug.log(notice);
            debug.log(param);
            debug.log('error event');
            var e = self.initEventObj(self.events.error, {
                'code': code,
                'notice': notice,
                'param': param
            });
            document.dispatchEvent(e);
        });

        this.socket.on('rtspio.beginning', function (notice, param) {
            debug.log(notice);
            debug.log(param);
            debug.log('beginning event');
            var e = self.initEventObj(self.events.beginningOfStream, {
                'notice': notice,
                'param': param
            });
            document.dispatchEvent(e);
        });

        this.socket.on('rtspio.ended', function (notice, param) {
            debug.log(notice);
            debug.log(param);
            debug.log('ended event');
            var e = self.initEventObj(self.events.endOfStream, {
                'notice': notice,
                'param': param
            });
            document.dispatchEvent(e);
        });

        this.socket.on('rtspio.loaded', function (notice, param) {
            debug.log(notice);
            debug.log(param);
            debug.log('loaded event');
            var e = self.initEventObj(self.events.loaded, {
                'notice': notice,
                'param': param
            });
            document.dispatchEvent(e);
        });

        this.socket.on('rtspio.playing', function (notice, param) {
            debug.log('playing event');
            debug.log(notice);
            debug.log(param);
            var e = self.initEventObj(self.events.playing, {
                'notice': notice,
                'param': param
            });
            document.dispatchEvent(e);
        });

        this.socket.on('rtspio.paused', function (notice, param) {
            debug.log('paused event');
            debug.log(notice);
            debug.log(param);
            var e = self.initEventObj(self.events.paused, {
                'notice': notice,
                'param': param
            });
            document.dispatchEvent(e);
        });

        this.socket.on('rtspio.currentTime', function (notice, param) {
            debug.log('currentTime event');
            debug.log(notice);
            debug.log(param);
            if (param.indexOf('Position:') >= 0) {
                var lines = param.trim().split('\r\n');
                param = {};
                for (var i = 0; i < lines.length; i++) {
                    var pair = lines[i].trim().split(':');
                    if (pair.length > 1) {
                        param[pair[0].trim()] = pair[1].trim();
                    }
                }
            }
            var e = self.initEventObj(self.events.currentTime, {
                'notice': notice,
                'param': param
            });
            document.dispatchEvent(e);
        });

        this.socket.on('rtspio.announced', function (notice, param) {
            debug.log('announced event');
            debug.log(notice);
            debug.log(param);
            var e = self.initEventObj(self.events.announced, {
                'notice': notice,
                'param': param
            });
            document.dispatchEvent(e);
        });

        this.socket.on('rtspio.teardown', function (notice, param) {
            debug.log('teardown event');
            debug.log(notice);
            debug.log(param);
            debug.log('We received the Teardown Callback');
            var e = self.initEventObj(self.events.teardown, {
                'notice': notice,
                'param': param
            });
            document.dispatchEvent(e);
            self.kill();
        });
    },

    // EMITTERS
    // we should only call these internally.

    _emitHandshake: function () {
        debug.log('io handshake');
        this.socket.emit('handshake', this.ip, this.roomNumber, this.device);
    },

    _emitLoad: function () {
        debug.log('io load');
        this.socket.emit('load', this.url);
    },

    _emitPlay: function (scale, offset) {
        debug.log('io play');
        this.socket.emit('play', scale, offset);
    },

    _emitPause: function () {
        debug.log('io pause');
        this.socket.emit('pause');
    },

    _emitCurrentTime: function () {
        debug.log('io currentTime');
        this.socket.emit('currentTime');
    },

    _emitTeardown: function () {
        debug.log('io teardown');
        this.socket.emit('teardown');
    },

    // TRIGGER EMITTER
    // these will be called externally

    play: function (scale, offset) {
        debug.log('video play, scale=' + scale + ' offset=' + offset);
        this._emitPlay(scale, offset);
    },

    pause: function () {
        debug.log('video pause');
        this._emitPause();
    },

    teardown: function () {
        debug.log('video teardown');
        this._emitTeardown();
    },

    getCurrentTime: function () {
        debug.log('video getCurrentTime');
        this._emitCurrentTime();
    },

    load: function (url) {
        this.url = url || this.url;
        debug.log('loading stream ' + this.url);
        debug.log('destination ' + this.ip);
        this.connect();
    },

    kill: function () {
        this.disconnect();
    },

    init: function (obj) {
        var self = this;
        debug.log('Initialize client');
        self.setProperties(obj);

        // only get socket.io js once
        if (!window.io) {
            var url = this.proxyServerUrl + '/socket.io/socket.io.js';
            debug.log('loading socket.io lib from ' + url);
            $.getScript(url, function (data, textStatus, jqxhr) {
                debug.log('socket.io lib loaded');
                self.io = window.io;
                var e = self.initEventObj(self.events.initialized, {});
                document.dispatchEvent(e);
            });
        }
        else {
            debug.log('socket.io lib was already loaded previously');
            self.io = window.io;
        }
    },

    // SETTERS

    setProperties: function (obj) {
        for (var o in obj) {
            this[o] = obj[o];
        }
    },

    setIp: function (ip) {
        this.ip = ip;
    },

    setDevice: function (device) {
        this.device = device;
    },

    setUrl: function (url) {
        this.url = url;
    },

    setProxyServerUrl: function (psu) {
        this.proxyServerUrl = psu;
    },

    setRoomNumber: function (num) {
        this.roomNumber = num;
    }
};