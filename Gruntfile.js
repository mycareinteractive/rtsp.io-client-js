/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    xconfig: grunt.file.readJSON('grunt.config.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration.
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true
      },
      dist: {
        src: ['lib/<%= pkg.name %>.js'],
        dest: 'dist/<%= pkg.name %>.js'
      }
    },
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      dist: {
        options: {
          sourceMap: true,
          sourceMapName: 'bin/rtsp.io-client-js.map'
        },
        files: {
          'bin/<%= pkg.name %>.min.js': ['src/client.js']
        }
      }
    },
    jshint: {
      options: {
        curly: true,
        devel: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: false,
        unused: false,
        boss: true,
        eqnull: true,
        browser: true,
        globals: {
          'jQuery': true,
          'io': true
        }
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib_test: {
        src: ['lib/**/*.js', 'test/**/*.js']
      },
      client: {
        src: [
          'src/**/*.js',
          'public/javascripts/*.js'
        ]
      }
    },
    qunit: {
      files: ['test/**/*.html']
    },
    copy: {
      toRtspIo: {
        files: [
          // includes files within path
          {expand: true, cwd: 'bin/', src: ['rtsp.io-client-js.min.js','rtsp.io-client-js.map'], dest: '<%= xconfig.rtspProxy %>/public/js/', filter: 'isFile'},
          {expand: true, cwd: 'public/javascripts/', src: ['player.js'], dest: '<%= xconfig.rtspProxy %>/public/js/', filter: 'isFile'}
        ]
      },
      toPublic: {
        files: [
          // includes files within path
          {expand: true, cwd: 'bin/', src: ['rtsp.io-client-js.min.js','rtsp.io-client-js.map'], dest: 'public/javascripts/lib/', filter: 'isFile'}
        ]
      },
      toGrandview: {
        files: [
          {expand: true, cwd: 'bin/', src: ['rtsp.io-client-js.min.js','rtsp.io-client-js.map'], dest: '<%= xconfig.grandview %>/', filter: 'isFile'}
        ]
      }
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib_test: {
        files: '<%= jshint.lib_test.src %>',
        tasks: ['jshint:lib_test', 'qunit']
      },
      client: {
        files: [
          'src/client.js'
        ],
        tasks: ['jshint:client','uglify', 'copy:toPublic']
      },
      simulator: {
        files: [
          'views/**/*.ejs',
          'public/stylesheets/**/*.css',
          'public/javascripts/**/*.js'
        ],
        tasks: ['jshint:client'],
        options: {
          livereload: true
        }
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-bump');

  // Default task.
  grunt.registerTask('default', ['jshint', 'qunit', 'concat', 'uglify']);
  grunt.registerTask('build', ['jshint:client','uglify', 'copy:toPublic']);

};
